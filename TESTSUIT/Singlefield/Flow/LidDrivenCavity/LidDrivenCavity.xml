<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Lid-driven cavity flow - Stokes problem</title>
    <authors>
      <author>Simon Triebenbacher</author>
    </authors>
    <date>2012-03-09</date>
    <keywords>
      <keyword>CFD</keyword>
      <keyword>FluidMechPerturbedPDE</keyword>
    </keywords>    
    <references>
      @MASTERTHESIS{Link2004,
      author = {Gerhard Link},
      title = {Numerical Simulation of Fluid-Structure Interaction},
      school = {University Erlangen-Nuremberg},
      year = {2004},
      month = dec,
      owner = {simon},
      timestamp = {2009.04.24},
      }
    </references>
    <isVerified>no</isVerified>
    <description>cf. theory.pdf</description>
  </documentation>
  
  
  <fileFormats>
    <input>
      <!--<gmsh fileName="LidDrivenCavity.msh"/>-->
      <hdf5 fileName="LidDrivenCavity.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane" printGridInfo="yes">
    <variableList>
      <var name="W"    value="1"/>
      <var name="H"    value="1"/>
    </variableList>
    <regionList>
      <region name="cavity" material="FluidMat"/>
    </regionList>
    
    <nodeList>
      <nodes name="pres_fix">
        <coord x="0.0" y="-H"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
       <gridOrder/> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="velIntegId">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="presIntegId">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <fluidMech systemId="fluid" formulation="perturbed">
        <regionList>
          <region name="cavity"/>
        </regionList>
        
        <bcsAndLoads>
          <noPressure name="pres_fix"/>

          <noSlip name="top">
            <comp dof="y"/>
          </noSlip>
           
          <noSlip name="bottom">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          
          <noSlip name="left">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          
          <noSlip name="right">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          
          <velocity name="top"> 
             <comp dof="x" value="-x*x*x*x*x*x*x*x*x*x + 1"/>
           </velocity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions/>         
          </nodeResult>
          <elemResult type="fluidMechStress">
            <allRegions/>
          </elemResult>
          <elemResult type="fluidMechStrainRate">
            <allRegions/>
          </elemResult>
          
          <!--sensorArray fileName="vel-line.txt" type="fluidMechVelocity">
            <parametric>
            <list comp="x" start="1" stop="1" inc="0"/>
            <list comp="y" start="0" stop="1" inc="0.01"/>
            </parametric>
          </sensorArray-->          
        </storeResults>
      </fluidMech>
    </pdeList>
    <linearSystems>
      <system id="fluid">
        <solutionStrategy>
          <standard>
            <setup idbcHandling="elimination" staticCondensation="no"/>
            <!--exportLinSys baseName="stokes_mat_vel"/-->
            <matrix storage="sparseNonSym"/>
            <solver id="default"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <umfpack/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
