<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation 
    file:/home/kroppert/Devel/CFS_SRC/latest_trunkGit/CFS/share/xml/CFS-Simulation/CFS.xsd">
    
    <documentation>
        <title>Nonlinear simulation with magnetic scalar potential (Psi formulation) </title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2022-06-30</date>
        <keywords>
            <keyword>magneticScalar</keyword>
        </keywords>
        <references></references>
        <isVerified>no</isVerified>
        <description> In this example, a totally artifical excitation of two current driven layers with an inbetween
                      nonlinear material is simulated. This is jused used to verify that the new mu(H) evaluation (
                      compared to the nu(B) evaluation for A-based magnetic simulations) is working correctly
        </description>
    </documentation>


    <!-- define which files are needed for simulation input & output-->
    <fileFormats>
        <input>
            <hdf5 fileName="MagScalarPot_Sandwich.h5ref"/>
        </input>
        <output>
            <hdf5 id="hdf5"/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <domain geometryType="3d">
        <variableList>
            <var name="i" value="0.01"/>
            <var name="N" value="250"/>
            <var name="f_coil" value="10"/>
            <var name="A" value="0.002"/>
        </variableList>
        <regionList>
            <region name="V_core" material="iron_analytic" />
            <region name="V_air_top" material="air"/>
            <region name="V_air_bottom" material="air"/>
        </regionList>
        <surfRegionList>
            <surfRegion name="S_z"/>
            <surfRegion name="S_y"/>
            <surfRegion name="S_x"/>
        </surfRegionList>
    </domain>

    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder> 0 </isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>

    <sequenceStep index="1">
        <analysis>
            <static></static>
        </analysis>

        <pdeList>
            <magneticEdge onlyVacuum="true" >
                <regionList>
                    <region name="V_core" polyId="Hcurl"/>
                    <region name="V_air_top" polyId="Hcurl"/>
                    <region name="V_air_bottom" polyId="Hcurl"/>
                </regionList>

                <bcsAndLoads>
                    <fluxParallel name="S_z_pos"/>
                    <fluxParallel name="S_z_neg"/>
                    <fluxParallel name="S_x_pos"/>
                    <fluxParallel name="S_x_neg"/>
                </bcsAndLoads>

                <coilList>
                    <coil id="coil1">
                        <source type="current" value="i*N"/>
                        <part id="1">
                            <regionList>
                                <region name="V_air_top"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="x" value="1.0"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="A"/>
                        </part>
                        <part id="2">
                            <regionList>
                                <region name="V_air_bottom"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="x" value="-1.0"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="A"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions outputIds="hdf5"/>
                    </elemResult>
                    <elemResult type="magFieldIntensity">
                        <allRegions outputIds="hdf5"/>
                    </elemResult>
                </storeResults>
            </magneticEdge>
        </pdeList>
    </sequenceStep>




    <sequenceStep index="2">
        <analysis>
            <static></static>
        </analysis>

        <pdeList>
            <magnetic formulation="Psi">
                <regionList>
                    <region name="V_core" polyId="H1" nonLinIds="nl1"/>
                    <region name="V_air_top" polyId="H1"/>
                    <region name="V_air_bottom" polyId="H1"/>
                </regionList>

                <nonLinList>
                    <permeability id="nl1" />
                </nonLinList>

                <bcsAndLoads>
                    <ground name="S_y_pos"/>
                    <potential name="S_y_neg" value="1"/>

                    <fieldIntensity name="V_air_top">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant  step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>

                    <fieldIntensity name="V_air_bottom">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant  step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>

                    <fieldIntensity name="V_core">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant  step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>
                </bcsAndLoads>

                <storeResults>
                    <elemResult type="magFieldIntensity">
                        <allRegions outputIds="hdf5"/>
                    </elemResult>
                    <elemResult type="magFluxDensity">
                        <allRegions />
                    </elemResult>
                    <nodeResult type="magPotential">
                        <allRegions />
                    </nodeResult>
                    <!--<elemResult type="magElemPermeability">
                        <regionList>
                            <region name="V_core" />
                        </regionList>
                    </elemResult> -->
                </storeResults>
            </magnetic>
        </pdeList>

        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <nonLinear logging="yes" method="fixPoint">
                            <lineSearch />
                            <incStopCrit>1e-2</incStopCrit>
                            <resStopCrit>1e-2</resStopCrit>
                            <maxNumIters>100</maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
            </system>
        </linearSystems>
    </sequenceStep>


</cfsSimulation>
