<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
        <title>3D straight inductor with global excitation</title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2019-06-26</date>
        <keywords>
            <keyword>magneticEdge</keyword>
        </keywords>
        <references></references>
        <isVerified>no</isVerified>
        <description> Between the sheet and air, a NC Nitsche interface is located
                      and we use global-current as well as -voltage excitation</description>
    </documentation>
    
    <fileFormats>
        <input>
            <hdf5 fileName="mesh.cfs"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <domain printGridInfo="yes" geometryType="3d">
        <variableList>
            <var name="a" value="6e-3"/>
            <var name="b" value="6e-3"/>
            <var name="A_ind" value="1"/>
            <var name="f_coil" value="1000"/>
            <var name="I_coil" value="6000"/>
            <var name="U_coil" value="60"/>
        </variableList>
        
        <regionList>
            <region name="V_inductor" material="copper"/>
            <region name="V_air" material="air"/>
            <region name="V_sheet" material="bandMat"/>
        </regionList>
        
        <surfRegionList>
            <surfRegion name="S_air_x"/>
            <surfRegion name="S_air_y"/>
            <surfRegion name="S_air_z"/> 
            <surfRegion name="S_sheet_xP"/>
            <surfRegion name="S_sheet_y"/>
            <surfRegion name="S_coil_in"/>
            <surfRegion name="S_coil_out"/>
            
            <surfRegion name="NC_air_xN"/>
            <surfRegion name="NC_sheet_xN"/>
            
            <surfRegion name="NC_air_zN"/>
            <surfRegion name="NC_sheet_zN"/>
            
            <surfRegion name="NC_air_zP"/>
            <surfRegion name="NC_sheet_zP"/>
        </surfRegionList>
        
        <ncInterfaceList>
            <ncInterface name="zP1" masterSide="NC_air_xN" slaveSide="NC_sheet_xN"/>
            <ncInterface name="zP2" masterSide="NC_air_zN" slaveSide="NC_sheet_zN"/>
            <ncInterface name="zP3" masterSide="NC_air_zP" slaveSide="NC_sheet_zP"/>
        </ncInterfaceList>
    </domain>
    
   
   
    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>
  
  
  
<!--
     ===========================================================================
                    Current Direction
     ===========================================================================
-->
    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <elecConduction>
                <regionList>
                    <region name="V_inductor" polyId="H1"/>
                </regionList>
                <bcsAndLoads>
                    <potential name="S_coil_in" value="1.0"/>
                    <ground name="S_coil_out"/>
                </bcsAndLoads>
                <storeResults>
                    <elemResult type="elecCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <allRegions/>
                    </nodeResult>
                    <regionResult type="elecGradVInt">
                        <allRegions/>
                    </regionResult>
                </storeResults>
            </elecConduction> 
        </pdeList>
                
        <linearSystems>
            <system>
                <solverList>
                    <pardiso/>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
    
    <!--
          ===========================================================================
                    Magnetic Edge PDE
     ===========================================================================
-->
    <sequenceStep index="2">
        <analysis>
            <harmonic>
                <numFreq>1</numFreq>
                <startFreq>f_coil</startFreq>
                <stopFreq>f_coil</stopFreq>
            </harmonic>
        </analysis>
        
        <pdeList>
            <magneticEdge formulation="specialA-V">
                <regionList>
                    <region name="V_inductor" polyId="Hcurl"/>
                    <region name="V_air" polyId="Hcurl"/>
                    <region name="V_sheet" polyId="Hcurl"/>
                </regionList>
                
                <ncInterfaceList>
                    <ncInterface name="zP1" formulation="Nitsche" nitscheFactor="100"/>
                    <ncInterface name="zP2" formulation="Nitsche" nitscheFactor="100"/>
                    <ncInterface name="zP3" formulation="Nitsche" nitscheFactor="100"/>
                </ncInterfaceList>
               
                
                <bcsAndLoads>
                    <fluxParallel name="S_air_x"/>
                    <fluxParallel name="S_air_y"/>
                    <fluxParallel name="S_air_z"/>
                    <fluxParallel name="S_sheet_xP"/>
                    <fluxParallel name="S_sheet_yN"/>
                    <fluxParallel name="S_sheet_yP"/>
                    <fluxParallel name="S_coil_in"/>
                    <fluxParallel name="S_coil_out"/>
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil1">
                        <source type="specialcurrent" value="I_coil"/>
                        <part id="1">
                            <regionList>
                                <region name="V_inductor"/>
                            </regionList>
                            <direction>
                                <external normalise="no">
                                    <sequenceStep index="1">
                                        <quantity name="elecCurrentDensity" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </direction>
                            <gradV_in_gradV>
                                <external>
                                    <sequenceStep index="1">
                                        <quantity name="elecGradVInt" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </gradV_in_gradV>
                            <wireCrossSection area="A_ind"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magJouleLossPowerDensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </magneticEdge>            
        </pdeList>
        
        <linearSystems>
            <system>
                <solverList>
                    <pardiso/>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
 

    <sequenceStep index="3">
        <analysis>
            <harmonic>
                <numFreq>1</numFreq>
                <startFreq>f_coil</startFreq>
                <stopFreq>f_coil</stopFreq>
            </harmonic>
        </analysis>
        
        <pdeList>
            <magneticEdge formulation="specialA-V">
                <regionList>
                    <region name="V_inductor" polyId="Hcurl"/>
                    <region name="V_air" polyId="Hcurl"/>
                    <region name="V_sheet" polyId="Hcurl"/>
                </regionList>
                
                <ncInterfaceList>
                    <ncInterface name="zP1" formulation="Nitsche" nitscheFactor="100"/>
                    <ncInterface name="zP2" formulation="Nitsche" nitscheFactor="100"/>
                    <ncInterface name="zP3" formulation="Nitsche" nitscheFactor="100"/>
                </ncInterfaceList>
                
                
                <bcsAndLoads>
                    <fluxParallel name="S_air_x"/>
                    <fluxParallel name="S_air_y"/>
                    <fluxParallel name="S_air_z"/>
                    <fluxParallel name="S_sheet_xP"/>
                    <fluxParallel name="S_sheet_yN"/>
                    <fluxParallel name="S_sheet_yP"/>
                    <fluxParallel name="S_coil_in"/>
                    <fluxParallel name="S_coil_out"/>
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil1">
                        <source type="specialvoltage" value="U_coil"/>
                        <part id="1">
                            <regionList>
                                <region name="V_inductor"/>
                            </regionList>
                            <direction>
                                <external normalise="no">
                                    <sequenceStep index="1">
                                        <quantity name="elecCurrentDensity" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </direction>
                            <gradV_in_gradV>
                                <external>
                                    <sequenceStep index="1">
                                        <quantity name="elecGradVInt" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </gradV_in_gradV>
                            <wireCrossSection area="A_ind"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magJouleLossPowerDensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </magneticEdge>            
        </pdeList>
        
        <linearSystems>
            <system>
                <solverList>
                    <pardiso/>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
</cfsSimulation>
