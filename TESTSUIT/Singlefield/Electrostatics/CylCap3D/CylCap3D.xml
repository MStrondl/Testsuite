<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  
  <documentation>
    <title>Cylindric Capacitance (3D setup)</title>
    <authors>
      <author>ahauck</author>
    </authors>
    <date>2011-12-12</date>
    <keywords>
      <keyword>electrostatic</keyword>
      <keyword>p-FEM-Legendre</keyword>
      <keyword>static condensation</keyword>
    </keywords>
    <references>
      Kories, Taschenbuch der Elektrotechnik,
      4th edition, p. 49
    </references>
    <isVerified>yes</isVerified>
    <description>
      This is 3D model of a cylindric capacitor,
      filled with two different dielectrica. We use a mixed quad, tria
      mesh with a very coarse element size. 
      
      To increase accuracy, a higher polynomial degree is chosen.
   
      For comparison, we calculate the capacitance using the
      total energy.
      In addition - when used with interpolation active - the
      electric field is written out on two lines (y=0, x=0).

      The analytical calculations can be found in the python / matplotlib
      bases calc.py script.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d" printGridInfo="yes">
    <variableList>
      <var name="r1"   value="1e-3"/>
      <var name="r2"   value="2e-3"/>
      <var name="r3"   value="5e-3"/>
      <var name="l"    value="1e-2"/>     
      <var name="eps1" value="1.992E-11"/> <!-- permittivity of PE -->
      <var name="eps2" value="8.859E-12"/> <!-- permittivity of air -->
      <var name="U"    value="1"/>         <!-- potential on hot electrode -->
    </variableList>
    <regionList>
      <region name="dielec-1" material="polyethylene"/>
      <region name="dielec-2" material="air"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="inner"/>
      <surfRegion name="outer"/>
    </surfRegionList>
  </domain>
  
  <fePolynomialList>
    <Legendre id="default">
       <isoOrder>3</isoOrder> 
    </Legendre>
  </fePolynomialList>
  
  
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <electrostatic systemId="elec">
        <regionList>
          <region name="dielec-1" polyId="default"/>
          <region name="dielec-2" polyId="default"/>
        </regionList>
        
        <bcsAndLoads>
          <ground name="outer"/>
          <potential name="inner" value="U"/>
        </bcsAndLoads>
        
        <storeResults>
          <!--<nodeResult type="elecPotential">
            <allRegions/>
          </nodeResult>-->
          <elemResult type="elecFieldIntensity">
            <allRegions/>
          </elemResult>
          <regionResult type="elecEnergy">
            <allRegions outputIds="txt,h5"/>
          </regionResult>
          <!-- Optional:
            If USE_INTERPOLATION is set to true, we can write out the elctric
            potential and field along a concentric line at half height -->
          
          
          <!--<sensorArray fileName="pot-line.txt" type="elecPotential">
            <parametric>
            <list comp="x" start="r1" stop="r3" inc="(r3-r1)/100"/>
            <list comp="y" start="r1/10" stop="r1/10" inc="0"/>
            <list comp="z" start="l/2" stop="l/2" inc="0"/>
            </parametric>
          </sensorArray>-->
          <!--sensorArray fileName="field-line-1.txt" type="elecFieldIntensity">
            <parametric>
            <list comp="x" start="r1" stop="r3" inc="(r3-r1)/100"/>
            <list comp="y" start="1e-5" stop="1e-5" inc="0"/>
            <list comp="z" start="l/2" stop="l/2" inc="0"/>
            </field>
            </parametric>
          <sensorArray fileName="field-line-2.txt" type="elecFieldIntensity">
          <parametric>
            <list comp="x" start="1e-5" stop="1e-5" inc="0"/>
            <list comp="y" start="r1" stop="r3" inc="(r3-r1)/100"/>
            <list comp="z" start="l/2" stop="l/2" inc="0"/>
            </parametric>
            </sensorArray>-->
        </storeResults>
      </electrostatic>
    </pdeList>
    <linearSystems>
      <system id="elec">
        <solutionStrategy>
          <standard>
            <!-- Note: we use static condensation to increase solver performance -->
            <setup idbcHandling="elimination" staticCondensation="yes"/>
            <solver id="default"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
