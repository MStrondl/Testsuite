<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Torque applied to two stacked cubes of nonmatching grids.</title>
    <authors>
      <author>Simon Triebenbacher</author>
    </authors>
    <date>2009-04-14</date>
    <keywords>
      <keyword>mortar fem</keyword>
      <keyword>nonmatching grids</keyword>
      <keyword>lagrange multiplier</keyword>
    </keywords>
    <references>
      none
    </references>
    <isVerified>yes</isVerified>
    <description>
      This XML file describes the mech-mech coupled nonmatching grid simulation
      of two stacked cubes with a nonmatching interface between them. The lower
      cube is fixed at the bottom and is considered as slave side. The upper cube
      is the master side and a force in phi-direction is applied on its top
      surface. Another component of this force pulls into positive z-direction.
      The mesh consists of HEXA27 and QUAD9 elements.       
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="torque3d_nc.h5" linearizeEntities="__none__"/>
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d">
    <regionList>
      <region name="master_elem" material="Aluminium"/>
      <region name="slave_elem" material="Aluminium"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="master_iface"/>
      <surfRegion name="slave_iface"/>
      <surfRegion name="top"/>
      <surfRegion name="bottom"/>
    </surfRegionList>
    <ncInterfaceList>
      <ncInterface name="nc_iface"
		   masterSide="master_iface"
		   slaveSide="slave_iface"
		   intersectionMethod="polygon"/>
    </ncInterfaceList>
    
    <coordSysList>
      <cylindric id="cylCoordSys">
      	<origin x="0.0" y="0.0" z="3.0"/>
      	<zAxis x="0.0" y="0.0" z="4.0"/>
      	<rAxis x="1.0" y="0.0" z="3.0"/>
      </cylindric>
    </coordSysList>
  </domain>
  
  <fePolynomialList>
    <Lagrange>
      <gridOrder/>
    </Lagrange>
  </fePolynomialList>
  
  <integrationSchemeList>
    <scheme>
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>
  </integrationSchemeList>
  
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <mechanic subType="3d">
      	<regionList>
      	  <region name="master_elem"/>
      	  <region name="slave_elem"/>
      	</regionList>
      	
      	<ncInterfaceList>
      	  <ncInterface name="nc_iface" formulation="Mortar"/>
      	</ncInterfaceList>
      	
      	<bcsAndLoads>
      	  <fix name="bottom">
      	    <comp dof="x"/>
      	    <comp dof="y"/>
      	    <comp dof="z"/>
      	  </fix>
      	  <rhsValues name="top">
      	    <comp dof="x" value="y*1e5"/>
      	    <comp dof="y" value="-x*1e5"/>
      	    <comp dof="z" value="1e5"/>
      	  </rhsValues>
      	</bcsAndLoads>
      	
      	<storeResults>
      	  <nodeResult type="mechDisplacement">
      	    <allRegions/>
      	  </nodeResult>
      	  <nodeResult type="mechRhsLoad">
      	    <allRegions/>
      	  </nodeResult>
      	  <elemResult type="mechStress">
      	    <allRegions/>
      	  </elemResult>
      	</storeResults>
      </mechanic>
      
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <setup idbcHandling="penalty"/>
            <exportLinSys baseName="fespace"/>
            <matrix storage="sparseNonSym" reordering="noReordering"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
            <posDef>no</posDef>
            <symStruct>no</symStruct>
            <IterRefineSteps>100</IterRefineSteps>
          </pardiso>
        </solverList>
      </system>
    </linearSystems>
    
  </sequenceStep>
  
</cfsSimulation>
