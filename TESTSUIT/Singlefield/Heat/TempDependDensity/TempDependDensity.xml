<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
  <documentation>
      <title>Test example for nonlinear heat conductivity, capacity and mass density</title>
      <authors>
          <author>Klaus Roppert</author>
      </authors>
      <date>2021-10-19</date>
          <keywords>
      <keyword>heatConduction</keyword>
    </keywords>
      <references>
          none
      </references>
      <isVerified> yes </isVerified>
      <description> none
      </description>
  </documentation>
  
  
  <fileFormats>
    <input>
      <cdb fileName="mesh.cdb" />
    </input>
    <output>
      <hdf5 id="hdf5" />
      <text id="txt" />
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d">
    <variableList>
      <var name="i_coil" value="1000" /> 
      <var name="f_coil" value="10000" /> 
      <var name="velocity" value="0.03"/>
      <var name="init_temp" value="293.15"/>
      <var name="wire_area" value="0.0006"/>
    </variableList>

    <regionList>
      <region name="V_COIL" material="copper" />
      <region name="V_ROD" material="50CrMo4" />
      <region name="V_AIR" material="air" />
    </regionList>
    
    <coordSysList>
      <cylindric id="cyl1">
        <origin x="0.0" y="0.0" z="0.0" />
        <zAxis x="0" y="1" z="0" />
        <rAxis x="1" y="0" z="0" />
      </cylindric>
    </coordSysList>
    
  </domain>
  <sequenceStep index="1">
    <analysis>
      <static></static>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="V_ROD"/>
        </regionList>

        <bcsAndLoads>
          <temperature name="BC_MT_ROD" value="init_temp" />
          <temperature name="BC_T_ROD" value="init_temp" />
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions />
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="2">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f_coil" />
        </frequencyList>
      </harmonic>
    </analysis>

    <pdeList>
      <magneticEdge>
        <regionList>
          <region name="V_AIR" />
          <region name="V_ROD" />
          <region name="V_COIL" />
        </regionList>

        <bcsAndLoads>
          <fluxParallel name="BC_M_SURF" />
          <fluxParallel name="BC_M_COIL_IN" />
          <fluxParallel name="BC_M_COIL_OUT" />
          <fluxParallel name="BC_MT_ROD"/>
        </bcsAndLoads>

        <coilList>
          <coil id="IND">
            <source type="current" value="i_coil" />
            <part id="c1">
              <regionList>
                <region name="V_COIL" />
              </regionList>
              <direction>
                <analytic coordSysId="cyl1">
                  <comp dof="phi" value="1.0" />
                </analytic>
              </direction>
              <wireCrossSection area="wire_area"/>
            </part>
          </coil>
        </coilList>
	<storeResults>
        </storeResults>
      </magneticEdge>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="3">
    <analysis>
      <transient>
        <numSteps>2</numSteps>
        <deltaT>0.05</deltaT>
      </transient>
    </analysis>

    <pdeList>
      <heatConduction>
        <regionList>
          <region name="V_ROD" velocityId="vRod" nonLinIds="nonLin"/>
        </regionList>
        
        <nonLinList>
          <heatCapacity id="nonLin"/>
          <heatConductivity id="nonLin"/>
          <density id="nonLin"/>
        </nonLinList>
        
        <velocityList>
          <velocity name="vRod" coordSysId="cyl1">
            <comp dof="z" value="velocity" />
          </velocity>
        </velocityList>

        <initialValues>
          <initialState>
            <sequenceStep index="1" />
          </initialState>
        </initialValues>

        <bcsAndLoads>
          <heatFlux name="BC_MT_ROD" value="0" />
          <heatTransport bulkTemperature="init_temp"
            heatTransferCoefficient="2" volumeRegion="V_ROD"
            name="BC_T_ROD" />

          <heatSourceDensity volumeRegion="V_ROD" name="V_ROD">
            <sequenceStep index="2">
              <quantity name="magJouleLossPowerDensity"
                pdeName="magneticEdge" />
              <timeFreqMapping>
                <constant />
              </timeFreqMapping>
            </sequenceStep>
          </heatSourceDensity>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="heatTemperature">
            <regionList>
              <region name="V_ROD"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <nonLinear logging="yes" method="newton">
              <lineSearch type="minEnergy" />
              <incStopCrit>1e-7</incStopCrit>
              <resStopCrit>1e-7</resStopCrit>
              <maxNumIters>25</maxNumIters>
            </nonLinear>
          </standard>
        </solutionStrategy>
      </system>
    </linearSystems>
    
  </sequenceStep>
</cfsSimulation>

