<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>Transient LinFlow-PDE including the first convective term caused by the ALE-framework</title>
    <authors>
      <author>dmayrhof</author>
    </authors>
    <date>2021-11-25</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      1D problem (plane wave in a channel) with pressure excitation. Due to the prescribed constant grid velocity, a simple analytical solution can be found for the case µ=0.
      We compare the resulting wave number with the analytical one given by
      k = \omega*(v_g+\sqrt(v_g^2+4*c_0^2))/(2*c_0^2)
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="PlaneWave2DGridVelC1.h5ref"/>
      <!--<cdb fileName="Channel_mesh.cdb"/>-->
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="Channel" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Excite"/>
      <surfRegion name="BC_Bot"/>
      <surfRegion name="BC_Top"/>
      <surfRegion name="BC_Right"/>
    </surfRegionList> 
    
    <nodeList>
      <nodes name="S1">
        <coord x="0" y="0" z="0"/>
      </nodes>
      <nodes name="S2">
        <coord x="0.5" y="0" z="0"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="integVel">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integPres">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>50</numSteps>
        <deltaT>5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" enableGridVelC1="true" enableGridVelC2="false">
        <regionList>
          <region name="Channel" movingMeshId="moveGridID"/>
        </regionList>
        
        <movingMeshList>
          <movingMesh name="moveGridID">
            <comp dof="x" value="50"/>
          </movingMesh>
        </movingMeshList>
        
        <bcsAndLoads> 
          <noSlip name="BC_Top">
            <comp dof="y"/>         
          </noSlip>
          <noSlip name="BC_Bot">
            <comp dof="y"/>         
          </noSlip>
          <pressure name="Excite" value="sin(2*pi*1000*t)*(1-exp(-t/(3e-3)))"/>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/> 
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
              <nodes name="S2" outputIds="txt"/>
            </nodeList>        
          </nodeResult>
          <elemResult type="fluidMechMeshVelocityElem">
            <allRegions outputIds="h5"/>
          </elemResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
