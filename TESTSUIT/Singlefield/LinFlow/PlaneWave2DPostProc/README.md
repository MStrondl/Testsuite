﻿## Analysis of the post-processing results for a plane wave propagating

###  Description

This test consists of a single channel where a plane wave is propagating. With no background-velocity, grid-velocity etc. an analytical solution can be found for the velocity field which is used to analytically calcualte the post-processing results (stress components).

