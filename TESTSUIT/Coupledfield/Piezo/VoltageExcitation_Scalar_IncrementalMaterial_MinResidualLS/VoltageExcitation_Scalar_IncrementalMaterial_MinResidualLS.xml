<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>2D plate capacitor with hysteretic dielectric/piezoelectric material between the plates </title>
    <authors>
      <author>Michael Nierla</author>
    </authors>
    <date>2019-07-09</date>
    <keywords>
      <keyword>piezo</keyword>
      <keyword>electrostatic</keyword>
      <keyword>hysteresis</keyword>
      <keyword>linesearch</keyword>
      <keyword>transient</keyword>
    </keywords>
    <references>none</references>
    <isVerified>no</isVerified>
    <description>
      Geometry:
        Simple plate capacitor (size 3 m x3 m)
        Left and right hand edges are electrodes
        Top and bottom edges are default boundaries
      
      Sketch:
      
      y-axis
      |
      |___________________ air
      |                   |
      |                   |				
      |                   |
      |                   |
      |     dielectric/   |
      |     piezoelectric |
      |     material      |
      |                   |      
      |___________________|_______ x-axis
      
      Excitation:
        Left hand edge: Potential excitation
        Right hand edge: Ground
                  
      Aims of this test case:
      - Apply hysteresis model to piezo-coupled case
      - Compute irreversible strains and stresses
      - Use separate hysteresis operator for strain-computation
      - Include deltaS/deltaE in computation of secant-matrix (see M. Kaltenbacher) [CURRENTLY NOT WORKING]
                    
    </description>
  </documentation>

  <fileFormats>
    <input>
      <hdf5 fileName="nineElement.h5"/>
    </input> 
    <output>
      <hdf5/>
       <text id="txt1" fileCollect="entity"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">

    <regionList>
      <region name="probe" material="Pic255-ScalarPreisach"/>
    </regionList>

    <surfRegionList>
      <surfRegion name="left"/>
      <surfRegion name="right"/>
      <surfRegion name="top"/>
      <surfRegion name="bot"/>
    </surfRegionList>
    
    <elemList>
      <elems name="center"></elems>
    </elemList>
  </domain>

  <sequenceStep>

    <analysis>
      <transient>
        <numSteps>120</numSteps>
        <deltaT>2</deltaT>
      </transient>
    </analysis>

	<pdeList>
      <electrostatic>
        <regionList>
		      <region name="probe" nonLinIds="h"/>
        </regionList>
     
        <nonLinList>
          <hysteresis id="h"/>
        </nonLinList>
                
        <bcsAndLoads>
<!--          <charge name="left" value="0.6*sample1D('sig1.txt',t,1)"></charge>
          <constraint name="left"/>-->
          <ground name="right"/>
           <potential name="left"  value="15e6*sample1D('sig1.txt',t,1)"/>
  <!--        <fieldParallel name="top" volumeRegion="probe"/>
          <fieldParallel name="bot" volumeRegion="probe"/>-->
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions/>
          </nodeResult>
          
          <elemResult type="elecFieldIntensity">        
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt1"/>
            </elemList>
          </elemResult>
          
          <elemResult type="elecPolarization">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt1"/>
            </elemList>
          </elemResult>

          <elemResult type="elecFluxDensity">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt1"/>
            </elemList>
          </elemResult>

        </storeResults>

      </electrostatic>
	  
	  
	  <mechanic subType="planeStrain">
	    <regionList>
	      <region name="probe"/>
	    </regionList>
	    
	    <bcsAndLoads>
	      <fix name="left">
	        <comp dof="x"/>
	        <comp dof="y"/>
	      </fix>
	      
	    </bcsAndLoads>
	    
	    <storeResults>
	      <nodeResult type="mechDisplacement">
	        <allRegions/>
	      </nodeResult>
	    </storeResults>
	    
	  </mechanic>
	  
	  
	</pdeList>
    
    <couplingList tag="anyTag">
      <direct>
        <piezoDirect>
          <regionList>
            <region name="probe"/>
          </regionList>
          <storeResults>
            <elemResult type="mechIrrStress">
              <allRegions/>
              <elemList>
                <elems name="center" outputIds="txt1"/>
              </elemList>
            </elemResult>
            <elemResult type="mechIrrStrain">
              <allRegions/>
              <elemList>
                <elems name="center" outputIds="txt1"/>
              </elemList>
            </elemResult>
          </storeResults>
        </piezoDirect>
        
      </direct>
    </couplingList>
    
        
   <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <hysteretic loggingToFile="yes" loggingToTerminal="2">
              <solutionMethod>
                <!-- all implemented solution methods aim at modifing the system matrix to allow for a convergent non-linear scheme;
                  this is done by adapting the material law on element level, i.e., prior to the assembly process; for fixpoint methods
                  this ends up in using a special fixpoint permittivity in the material law D = eps E + P and in the quasi Newton case, 
                  an approximation of the derivative dP/dE is used; 
                  in the directly coupled case, we not only have to assemble the electrostatic system but also the mechanical system and
                  the coupling matrices; currently, neither of these two terms is considered explicitely in the fixpoint methods and in the
                  finite difference Newton methods; the only implemented method (so far), that could be used to include dS/dE in mechanics and the
                  coupling terms is the secant method aka the incremental material method (see M. Kaltenbacher); 
                  dS/dE can be activated in the mechanical stiffness matrix by setting includeDeltaStrain to true;
                  it can furthermore be included in the computation of the coupling tensors by setting useDeltaInCouplingTensor to true;
                  unfortunately, there seem to be issues with these implementations at the moment; this particular setup for example runs better
                  if only dP/dE is used
                -->
                <QuasiNewton_SecantMethod includeDeltaStrain="no" useDeltaInCouplingTensor="no" initialNumberFPSteps="1" towardsPreviousTimestep="yes" calculateDeltaMatAtMidpointOnly="yes"/>
              </solutionMethod>
              <lineSearch>
                <Backtracking_SmallestResidual>
                  <maxIterLS>25</maxIterLS>
				  <!-- actuall works better if we allow for eta > 1! overrelaxation! -->
                  <etaStart>8.0E0</etaStart>
                  <etaMin>1.0E-3</etaMin>
                  <decreaseFactor>5.0E-1</decreaseFactor>
                  <residualDecreaseForSuccess>1.0E-1</residualDecreaseForSuccess>
                  <stoppingTol>1.0E-5</stoppingTol>
                  <!-- for this setup, we perform better with overrelaxation; otherwise increase number of outer iterations (parameter maxIter below) beyound 50 -->
                  <testOverrelaxation>no</testOverrelaxation>
                </Backtracking_SmallestResidual>
                
<!--                <None></None> -->
              </lineSearch>
              <stoppingCriteria>
                <increment_relative value="2.5E-6" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
                <residual_relative value="1.25E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
              </stoppingCriteria>
              <evaluationDepth>
                <evaluateAtIntegrationPoints_OneOperatorPerElement></evaluateAtIntegrationPoints_OneOperatorPerElement>
              </evaluationDepth>
              <failbackCriteria>
                <residual_relative value="7.5E-6" firstCheck="10" checkingFrequency="2" scaleToSystemSize="no"/>
                <residual_absolute value="2.5E-6" firstCheck="3" checkingFrequency="3" scaleToSystemSize="yes"/>
              </failbackCriteria>
              <maxIter>65</maxIter>
            </hysteretic>
            
          </standard>
        </solutionStrategy>
      </system>
    </linearSystems>

  </sequenceStep>
</cfsSimulation>
