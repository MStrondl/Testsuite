<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/master/CFS-Simulation/CFS.xsd"
    xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
        <title>Testcase for electrostatic forces</title>
        <authors>
            <author>Georg Jank</author>
        </authors>
        <date>2020-11-02</date>
        <keywords>
            <keyword>electrostatic</keyword>
        </keywords>
        <references></references>
        <isVerified>yes</isVerified>
        <description>
            This test case consists of two conducting plates. Between
            the plates there is an air gap. The lower plate is fixed 
            and the upper plate is attached to a spring (i.e. material
            with poisson number 0). A harmonic voltage is applied 
            across the plates and the electrostatic forces moves the 
            upper plate towards the lower plate. The aim of this 
            Testcases is to test the calculation of electrostatic 
            forces in a 3D model with complex numbers.
        </description>
    </documentation>
    
    <fileFormats>
        <input>
            <cdb fileName="ElecForcesCapacitorHarmonic3D.cdb"/>
        </input>
        <output>
            <hdf5/>
            <!--<text/>-->
        </output>
        <materialData file="mat.xml"/>
    </fileFormats>
    
    <domain geometryType="3d">
        <regionList>
            <region name="V_air" material="air"></region>
            <region name="V_upper_electrode" material="solid"></region>
            <region name="V_lower_electrode" material="solid"></region>
            <region name="V_spring" material="spring"></region>
        </regionList>
        <surfRegionList>
            <surfRegion name="S_upper_electrode"/>
            <surfRegion name="S_lower_electrode"/>
        </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Lagrange id="Lagrange1">
            <isoOrder>1</isoOrder>
        </Lagrange>
        <Lagrange id="Lagrange2">
            <isoOrder>2</isoOrder>
        </Lagrange>
    </fePolynomialList>
    
    <sequenceStep index="1">
        <analysis>
            <harmonic>
                <frequencyList>
                    <freq value="1000"/>
                </frequencyList>
            </harmonic>
        </analysis>
        <pdeList>
            <electrostatic>
                <regionList>
                    <region name="V_air"/>
                </regionList>
                <bcsAndLoads>
                    <potential name="S_upper_electrode" value="1"></potential>
                    <ground name="S_lower_electrode"></ground>
                </bcsAndLoads>
                <storeResults>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <surfElemResult type="elecForceDensity">
                        <surfRegionList>
                            <surfRegion name="S_upper_electrode"/>
                        </surfRegionList>
                    </surfElemResult>
                    <surfElemResult type="elecSurfaceChargeDensity">
                        <surfRegionList>
                            <surfRegion name="S_upper_electrode"/>
                        </surfRegionList>
                    </surfElemResult>    
                    <elemResult type="elecEnergyDensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </electrostatic>
        </pdeList>
    </sequenceStep>
    
    <sequenceStep index="2">
        <analysis>
            <harmonic>
                <frequencyList>
                    <freq value="1000"/>
                </frequencyList>
            </harmonic>
        </analysis>
        <pdeList>
            <mechanic subType="3d">
                <regionList>
                    <region name="V_upper_electrode"/>
                    <region name="V_spring" dampingId="springdamping"/>
                </regionList>
                <dampingList>
                    <rayleigh id="springdamping">
                        <adjustDamping>no</adjustDamping>
                    </rayleigh>
                </dampingList>
                <bcsAndLoads>
                    <traction name="S_upper_electrode"> 
                        <sequenceStep index="1">
                            <quantity name="elecForceDensity" pdeName="electrostatic"/>
                            <timeFreqMapping>
                                <constant/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </traction>
                    <fix name="S_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                    <fix name="V_upper_electrode">
                        <comp dof="x"/>
                        <comp dof="z"/>
                    </fix>
                </bcsAndLoads> 
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                        <!--<nodeList>
                            <nodes name="sensor"/>
                        </nodeList>-->
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>
    
</cfsSimulation>
