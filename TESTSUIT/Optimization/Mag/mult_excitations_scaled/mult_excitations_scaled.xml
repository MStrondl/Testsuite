<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>Minimize the integral over B^2 in the Ferrite without loosing a lot of magnetic coupling</title>
    <authors>
      <author>Fabian Wein/ Katharina Angermeier</author>
    </authors>
    <date>2019-09-13</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>Katharinas Masterthesis</references>
    <isVerified>no</isVerified>
    <description> Minimize the integral over B^2 in the Ferrite without dropping below a magnetic coupling of k^2 = 0.5 between the two coils.
    There are three excitations required, the first two for the coupling and the third with an excitation in both coils to calculate the integral
    over B^2, which is a measure for the losses. </description>
  </documentation>

  <fileFormats>
    <input>
      <gmsh fileName="coarse.msh" />
    </input>
    <output>
      <hdf5 />
      <text id="txt" />
    </output>
    <materialData file="Mat-stecker.xml" format="xml" />
  </fileFormats>

  <!-- material assignment -->
  <domain geometryType="axi">
    <variableList>
      <!-- define used varaibles here -->
      <var name="Durchflutung" value="3" />
      <var name="Area" value="4.6000e-05"></var>
      <var name="N" value="15"></var>
    </variableList>
    <regionList>
      <region name="coil_in" material="Copper" />
      <region name="coil_out" material="Copper" />
      <region name="ferrite" material="bh" />
      <region name="air" material="air" />
    </regionList>
  </domain>
  <sequenceStep index="1">
    <analysis>
      <!-- <harmonic> -->
      <!-- <frequencyList> -->
      <!-- <freq value="50000"/> -->
      <!-- </frequencyList> -->
      <!-- </harmonic> -->
      <static />
    </analysis>
    <pdeList>
      <magnetic>
        <regionList>
          <region name="coil_in" />
          <region name="coil_out" />
          <region name="ferrite" />
          <region name="air" />
        </regionList>
        <bcsAndLoads>
          <fluxParallel name="FluxParallel_h">
            <comp dof="r" />
          </fluxParallel>
          <fluxParallel name="FluxParallel_v">
            <comp dof="z" />
          </fluxParallel>
        </bcsAndLoads>
        <coilList>
          <coil id="primary">
            <source type="current" value="-Durchflutung*N" />
            <part id="1">
              <regionList>
                <region name="coil_in" />
              </regionList>
              <direction>
                <analytic>
                  <comp dof="z" value="1" />
                </analytic>
              </direction>
              <wireCrossSection area="Area/N" />
            </part>
          </coil>
          <coil id="secondary">
            <source type="current" value="Durchflutung*N" />
            <part id="2">
              <regionList>
                <region name="coil_out" />
              </regionList>
              <direction>
                <analytic>
                  <comp dof="z" value="1" />
                </analytic>
              </direction>
              <wireCrossSection area="Area/N" />
            </part>
          </coil>
        </coilList>
        <storeResults>
          <nodeResult type="magPotential">
            <allRegions />
          </nodeResult>
          <nodeResult type="magRhsLoad">
            <allRegions />
          </nodeResult>
          <nodeResult type="elecPotential">
            <allRegions />
          </nodeResult>
          <elemResult type="magFluxDensity">
            <allRegions />
          </elemResult>
          <elemResult type="magFieldIntensity">
            <allRegions />
          </elemResult>
          <elemResult type="magElemPermeability">
            <allRegions />
          </elemResult>
          <elemResult type="magCoilCurrentDensity">
            <allRegions />
          </elemResult>
          <elemResult type="magCoreLossDensity">
            <allRegions />
          </elemResult>
          <elemResult type="elecFieldIntensity">
            <allRegions />
          </elemResult>
          <elemResult type="magEddyCurrentDensity">
            <allRegions />
          </elemResult>
          <elemResult type="physicalPseudoDensity">
            <allRegions />
          </elemResult>
          <elemResult type="pseudoDensity">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_1">
            <regionList>
              <region name="ferrite" />
            </regionList>
          </elemResult>
        </storeResults>
      </magnetic>
    </pdeList>
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <!-- <exportLinSys solution="true"/> -->
            <matrix reordering="noReordering" />
            <!-- <nonLinear logging="yes" method="fixPoint"> -->
            <!-- <lineSearch type="minEnergy" /> -->
            <!-- <incStopCrit>1E-2</incStopCrit> -->
            <!-- <resStopCrit>1E-2</resStopCrit> -->
            <!-- <maxNumIters>100</maxNumIters> -->
            <!-- </nonLinear> -->
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso />
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>

  <optimization>
    <costFunction type="sqrMagFluxDensRZ" task="minimize" multiple_excitation="true" excitation="2" region="ferrite">
      <multipleExcitation>
        <excitations>
          <excitation>
            <coil id="primary" />
          </excitation>
          <excitation>
            <coil id="secondary" />
          </excitation>
          <excitation>
            <coil id="primary" />
            <coil id="secondary" />
          </excitation>
        </excitations>
      </multipleExcitation>

      <stopping queue="100" value="1.0E-4" type="designChange" />
    </costFunction>

    <constraint type="volume" bound="upperBound" value="0.5" mode="constraint" linear="true"/>
    <constraint type="magCoupling" bound="upperBound" value="-0.5" mode="constraint" excitation="0_1" region="ferrite"/>

    <optimizer type="snopt" maxIterations="10">
      <scale manual="100000"/>
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="1e-10" />
        <option name="verify_level" type="integer" value="-1" />
      </snopt>
    </optimizer>

    <ersatzMaterial material="magnetic" method="simp">
      <regions>
        <region name="ferrite" />
      </regions>

      <!-- currently necessary to switch off due to a bug in lec -->
      <designSpace local_element_cache="false" />
      <design name="density" initial="0.5" upper="1.0" lower="0.0" />
      <transferFunction type="simp" application="magnetic" param="1" />
      <result value="costGradient" id="optResult_1" />
      <!-- <result value="costGradient" id="optResult_2" detail="finiteDiffCostGradRelError"/> -->
      <!-- <result value="costGradient" id="optResult_3" detail="finiteDiffCostGrad"/> -->
      <export write="iteration" save="last" />
    </ersatzMaterial>
    <!-- write also adjoint solution at .5 timesteps, take care when reading pyhsical data! -->
    <commit mode="each_forward" stride="1" />
  </optimization>

</cfsSimulation>
