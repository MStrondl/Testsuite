<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>spaghetti 3D cantilever example with isotropic material</title>
    <authors>
      <author>Jannis Greifenstein</author>
    </authors>
    <date>2023-04-26</date>
    <references>spaghetti paper Greifenstein, Stingl, Wein for 2D (https://doi.org/10.1007/s00158-023-03534-8)</references>
    <isVerified>no</isVerified>
    <description>Numerical example of a loaded cantilever. Feature mapping using linear 3D spline ("spaghetti") and isotropic material
    Test mesh was created with create_mesh.py --type bulk3d --res 10
    </description>
  </documentation>

  <!-- In the given script the functions called from optimization are implemented. -->
  <python file="spaghetti3d.py" path="cfs:share:python" /> 
  
  <fileFormats>
    <input> 
      <hdf5 fileName="spaghetti3d_iso.h5ref"/>
    </input>
    <output>
      <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region material="99lines" name="mech"/>
    </regionList>
    <nodeList>
      <nodes name="load">
        <coord x="1.0" y="0.5" z="0.5"/>
      </nodes>
    </nodeList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="mech"/>
        </regionList>

        <bcsAndLoads>
           <fix name="left"> 
              <comp dof="x"/> <comp dof="y"/> <comp dof="z"/> 
           </fix>
           <force name="load" >
             <comp dof="z" value="1"/>
           </force>
           
        </bcsAndLoads>
        <storeResults>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
        <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_3">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_4">
            <allRegions/>
          </elemResult>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
  </sequenceStep>
    
  <optimization>
    <costFunction type="compliance" task="minimize" sequence="1" linear="false">
      <stopping queue="55" value="0.0001" type="relativeCostChange" />
    </costFunction>


    <constraint type="volume" bound="upperBound" value="0.2" mode="constraint" linear="false"/>
    <constraint type="localPython" value="0" bound="lowerBound" linear="false" mode="constraint"  >
      <python name="arc_overlap" sparsity="cfs_get_sparsity_arc_overlap" eval="cfs_get_constraint_arc_overlap" grad="cfs_get_gradient_arc_overlap" script="kernel" />  
    </constraint>
   
    <optimizer type="snopt" maxIterations="4">
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="1e-8"/>
        <option type="integer" name="verify_level" value="-1"/>
      </snopt>
    </optimizer>
    
    <ersatzMaterial region="mech" material="mechanic" method="spaghetti">
      <spaghetti combine="softmax" p="6" boundary="poly" transition=".16" order="2">
        <python file="spaghetti3d.py" path="cfs:share:python" >
<!--          <option key="vtk_lists" value="test"/>     -->
          <option key="gradient_check" value="0"/>
          <option key="silent" value="1"/>          
        </python> 
        <noodle segments="2">
          <node dof="x" initial="0.0" lower="0" upper="1" tip="start"/>
          <node dof="y" initial="0.5" lower="0" upper="1" tip="start" />
          <node dof="z" initial="0.4" lower="0" upper="1" tip="start" />
          <node dof="x" initial="1" lower="0" upper="1" tip="end" />
          <node dof="y" initial="0.50" lower="0" upper="1" tip="end" />
          <node dof="z" initial="0.5" lower="0" upper="1" tip="end" />
<!--          <inner dof="x" initial=".4" lower="0" upper="1" />
          <inner dof="y" initial=".45" lower="0" upper="1" />
          <inner dof="z" initial=".55" lower="0" upper="1" />-->
          <profile initial=".1" lower="0.1" upper=".5" />
          <radius initial=".3" lower="0.05" upper="5" />
          <alpha initial="0.99" lower="0" upper="1" />
        </noodle>
        <noodle segments="2">
          <node dof="x" initial="0.0" lower="0" upper="1" tip="start"/>
          <node dof="y" initial="0.5" lower="0" upper="1" tip="start" />
          <node dof="z" initial="0.6" lower="0" upper="1" tip="start" />
          <node dof="x" initial="1.0" lower="0" upper="1" tip="end" />
          <node dof="y" initial="0.5" lower="0" upper="1" tip="end" />
          <node dof="z" initial="0.5" lower="0" upper="1" tip="end" />
<!--          <inner dof="x" initial=".4" lower="0" upper="1" />
          <inner dof="y" initial=".45" lower="0" upper="1" />
          <inner dof="z" initial=".55" lower="0" upper="1" />-->
          <profile initial=".1" lower=".05" upper=".3"  />
          <radius initial=".3" lower="0.05" upper="5" />
          <alpha initial="0.99" lower="0" upper="1" />
        </noodle>        
        <noodle segments="2">
          <node dof="x" initial="0.0" lower="0" upper="1" tip="start"/>
          <node dof="y" initial="0.0" lower="0" upper="1" tip="start" />
          <node dof="z" initial="1.0" lower="0" upper="1" tip="start" />
          <node dof="x" initial="1.0" lower="0" upper="1" tip="end" />
          <node dof="y" initial="0.5" lower="0" upper="1" tip="end" />
          <node dof="z" initial="0.5" lower="0" upper="1" tip="end" />
<!--          <inner dof="x" initial=".2" lower="0" upper="1" />
          <inner dof="y" initial=".6" lower="0" upper="1" />
          <inner dof="z" initial=".1" lower="0" upper="1" />-->
          <profile initial=".1" lower=".05" upper=".3"  />
          <radius initial=".3" lower="0.05" upper="5" />
          <alpha initial="0.99" lower="0" upper="1" />
        </noodle>
        <noodle segments="2">
          <node dof="x" initial="0.0" lower="0" upper="1" tip="start"/>
          <node dof="y" initial="1" lower="0" upper="1" tip="start" />
          <node dof="z" initial="1" lower="0" upper="1" tip="start" />
          <node dof="x" initial="1" lower="0" upper="1" tip="end" />
          <node dof="y" initial="0.5" lower="0" upper="1" tip="end" />
          <node dof="z" initial="0.5" lower="0" upper="1" tip="end" />
<!--          <inner dof="x" initial=".9" lower="0" upper="1" />
          <inner dof="y" initial=".4" lower="0" upper="1" />
          <inner dof="z" initial=".8" lower="0" upper="1" />-->
          <profile initial=".1" lower=".05" upper=".3"  />
          <radius initial=".3" lower="0.05" upper="5" />
          <alpha initial="0.99" lower="0" upper="1" />
        </noodle>
      </spaghetti> 
    
      <design name="density" initial=".5" physical_lower="0" upper="1.0" bimaterial="weak"/>

      <transferFunction type="simp" application="mech" param="3"/>
      <result value="costGradient" id="optResult_1" /> 
      <result value="design" id="optResult_1" design="density" />
      <!--    <result value="design" id="optResult_2" design="rotAngle"/> -->
      <export save="all" write="iteration"/>
      
    </ersatzMaterial>
    <commit mode="forward" stride="1"/>
  </optimization>
</cfsSimulation>
